import importlib.resources

import parse_earlyprint_eebo_tcp.parse
import parse_earlyprint_eebo_tcp.tests.resources


def test_parse():
  with importlib.resources.open_text(parse_earlyprint_eebo_tcp.tests.resources, 'A50919.xml') as A50919:
    parse_earlyprint_eebo_tcp.parse.parse(A50919)
