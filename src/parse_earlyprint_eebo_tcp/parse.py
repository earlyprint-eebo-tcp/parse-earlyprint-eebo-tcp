from lxml import etree  # This is the only part of lxml we need


def parse(fileToParse):
  if isinstance(fileToParse, str) and not fileToParse.endswith('.xml'):
    raise ValueError(fileToParse)
  # First create a parser object. It will work faster if you tell it not to collect IDs.
  parser = etree.XMLParser(collect_ids=False)
  # Parse your XML file into a "tree" object
  return etree.parse(fileToParse, parser)
