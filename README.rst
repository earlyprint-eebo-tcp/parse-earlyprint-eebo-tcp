========
Overview
========

An example package. Generated with cookiecutter-pylibrary.

Installation
============

::

    pip install parse-earlyprint-eebo-tcp

You can also install the in-development version with::

    pip install https://gitlab.com/earlyprint-eebo-tcp/parse-earlyprint-eebo-tcp/-/archive/master/parse-earlyprint-eebo-tcp-master.zip


Documentation
=============


http://earlyprint-eebo-tcp.gitlab.io/parse-earlyprint-eebo-tcp


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
